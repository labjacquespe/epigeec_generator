#!/bin/bash

echo '2018-10 hg19 resource usage'
cd ../script/2018-10/hg19/

echo '--- MakeHdf5 ---'

echo "Job Wall-clock time"
ls *.out | grep "MakeHdf5" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Wall' | grep -oE '[0-9]{2}:[0-9]{2}:[0-9]{2}'

echo "Memory Utilized"
ls *.out | grep "MakeHdf5" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Memory Utilized' | grep -oE '[0-9]{1,3}\.[0-9]{1,2} .B'

echo "CPU Efficiency:"
ls *.out | grep "MakeHdf5" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'CPU Efficiency:' | grep -oE '[0-9]{1,2}\.[0-9]{1,2}\%'


echo '--- MakeDownResolution ---'

echo "seff order:"
ls *.out | grep "MakeDownResolution"

echo "Job Wall-clock time"
ls *.out | grep "MakeDownResolution" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Wall' | grep -oE '[0-9]{2}:[0-9]{2}:[0-9]{2}'

echo "Memory Utilized"
ls *.out | grep "MakeDownResolution" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Memory Utilized' | grep -oE '[0-9]{1,3}\.[0-9]{1,2} .B'

echo "CPU Efficiency:"
ls *.out | grep "MakeDownResolution" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'CPU Efficiency:' | grep -oE '[0-9]{1,2}\.[0-9]{1,2}\%'


echo '--- MakeFilter ---'

echo "seff order:"s
ls *.out | grep "MakeFilter"

echo "Job Wall-clock time"
ls *.out | grep "MakeFilter" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Wall' | grep -oE '[0-9]{2}:[0-9]{2}:[0-9]{2}'

echo "Memory Utilized"
ls *.out | grep "MakeFilter" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Memory Utilized' | grep -oE '[0-9]{1,3}\.[0-9]{1,2} .B'

echo "CPU Efficiency:"
ls *.out | grep "MakeFilter" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'CPU Efficiency:' | grep -oE '[0-9]{1,2}\.[0-9]{1,2}\%'


echo '--- MakeToRank ---'

echo "seff order:"
ls *.out | grep "MakeToRank"

echo "Job Wall-clock time"
ls *.out | grep "MakeToRank" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Wall' | grep -oE '[0-9]{2}:[0-9]{2}:[0-9]{2}'

echo "Memory Utilized"
ls *.out | grep "MakeToRank" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Memory Utilized' | grep -oE '[0-9]{1,3}\.[0-9]{1,2} .B'

echo "CPU Efficiency:"
ls *.out | grep "MakeToRank" | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'CPU Efficiency:' | grep -oE '[0-9]{1,2}\.[0-9]{1,2}\%'


echo '--- MakeCorrelate, pearson ---'

echo "seff order:"
ls *.out | grep -E "1kb.*MakeCorrelate" | grep 'pearson'

resolutions=("1kb" "10kb" "100kb" "1mb")
for resolution in "${resolutions[@]}"; do

    echo "---${resolution}---"

    echo "Job Wall-clock time"
    ls *.out | grep -E "${resolution}.*MakeCorrelate" | grep 'pearson' | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Wall' | grep -oE '[0-9]{2}:[0-9]{2}:[0-9]{2}'

    echo "Memory Utilized"
    ls *.out | grep -E "${resolution}.*MakeCorrelate" | grep 'pearson' | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'Memory Utilized' | grep -oE '[0-9]{1,3}\.[0-9]{1,2} .B'

    echo "CPU Efficiency:"
    ls *.out | grep -E "${resolution}.*MakeCorrelate" | grep 'pearson' | grep -oE '[0-9]{7}' | xargs -n1 seff | grep 'CPU Efficiency:' | grep -oE '[0-9]{1,2}\.[0-9]{1,2}\%'

done
