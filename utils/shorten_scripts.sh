#!/bin/bash

scripts_path=$1
assembly=$2
release=$3

local_data_path="/nfs3_ib/ip29/ip29/jacques_group/local_ihec_data/"${release}"/"${assembly}"/hdf5"

echo "Replacing paths in .sh scripts from ${scripts_path}"
echo "Given assembly: ${assembly}"
echo "Given release: ${release}"
echo "Replacing ${local_data_path}"

# Change paths for variables
sed -i 's@ /project/6007017/rabyj/epigeec/epigeec_generator/resource@ \$\{resource_path\}@g' ${scripts_path}/*Filter.sh
sed -i "s@ ${local_data_path}@ \$\{datapath\}@g" ${scripts_path}/*Filter.sh ${scripts_path}/*Rank.sh ${scripts_path}/*Resolution.sh

# Append variables at line 15
sed -i '15 a resource_path="/project/6007017/rabyj/epigeec/epigeec_generator/resource"' ${scripts_path}/*Filter.sh
sed -i "15 a datapath=\"${local_data_path}\"" ${scripts_path}/*Filter.sh ${scripts_path}/*Rank.sh ${scripts_path}/*Resolution.sh
