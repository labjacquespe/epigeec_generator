CONFIG = {
    'assembly': {
        # 'hg19': {
        #     'filter': [
        #         # all none is implicite
        #         ('all', 'blklst'),
        #         # ('gene', 'blklst'),
        #         # ('tss', 'blklst')
        #     ],
        #     'resolution': [
        #         # The highest is the first
        #         '1kb',
        #         # '10kb',
        #         # '100kb',
        #         # '1mb'
        #     ]
        # },
        'hg38': {
            'filter': [
                # ('gene', 'none'),
                # ('tss', 'none')
            ],
            'resolution': [
                # '1kb',
                # '10kb',
                # '100kb',
                '1mb'
            ]
        },
        # 'mm10': {
        #     'filter': [
        #         ('all', 'blklst')
        #     ],
        #     'resolution': [
        #         '1kb',
        #         # '10kb',
        #         # '100kb',
        #         # '1mb'
        #     ]
        # }
        # ,
        # 'saccer3': {
        #     'filter': [
        #     ],
        #     'resolution': [
        #         '100bp'
        #         '1kb',
        #         '10kb'
        #     ]
        # }
    },
    'metric': [
        'pearson',
        # 'spearman'
    ],
    'chromSizes': 'noy',
    # must be chronological order
    'date': [
        '2018-10',
        # '2016-11',
        # '2017-10'
    ]
}
