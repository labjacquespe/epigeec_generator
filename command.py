from provider import FilterProvider

TEMPLATE_RESOLUTION = {
    '100bp': 100,
    '1kb': 1000,
    '10kb': 10000,
    '100kb': 100000,
    '1mb': 1000000
}

class EpiGeecToRank(object):
    TO_RANK_TEMPLATE = 'epigeec-converter to-rank {source} {output}'

    def __init__(self, sources, outputs):
        self.sources = sources
        self.outputs = outputs

    def build(self):
        return [
            EpiGeecToRank.TO_RANK_TEMPLATE.format(
                source=src,
                output=out
            )
            for src, out in zip(self.sources, self.outputs)
        ]

class EpiGeecDownResolution(object):
    DOWN_RESOLUTION_TEMPLATE = 'epigeec-converter down-resolution {source} {resolution} {output}'

    def __init__(self, sources, resolution, outputs):
        self.sources = sources
        self.resolution = resolution
        self.outputs = outputs

    def build(self):
        return [
            EpiGeecDownResolution.DOWN_RESOLUTION_TEMPLATE.format(
                source=src,
                resolution=TEMPLATE_RESOLUTION[self.resolution],
                output=out
            )
            for src, out in zip(self.sources, self.outputs)
        ]

class EpiGeecToHdf5(object):
    TO_HDF5_TEMPLATE = 'epigeec to_hdf5 -bw {signal} {chromSizes} {resolution} {outHdf5} {detached}'

    def __init__(self, signalFiles, chromSizesFile, resolution, outHdf5Files, detached=False):
        self.signalFiles = signalFiles
        self.chromSizesFile = chromSizesFile
        self.resolution = resolution
        self.outHdf5Files = outHdf5Files
        self.detached = detached

    def build(self):
        detached_char = ''
        wait = ''
        if self.detached:
            detached_char = '&'
            wait = 'wait'

        return [
            EpiGeecToHdf5.TO_HDF5_TEMPLATE.format(
                signal=signal,
                chromSizes=self.chromSizesFile,
                resolution=TEMPLATE_RESOLUTION[self.resolution],
                outHdf5=hdf5,
                detached=detached_char
            )
            for signal, hdf5 in zip(self.signalFiles, self.outHdf5Files)
        ] + [
            wait
        ]

class EpiGeecFilter(object):
    FILTER_TEMPLATE = 'epigeec filter {select} {exclude} {hdf5} {chromSizes} {outHdf5}'

    def selectParam(self):
        filterFile = FilterProvider.getFileName(self.assembly, self.select)
        if filterFile:
            return '--select {filterFile}'.format(filterFile=filterFile)
        else:
            return ''
    
    def excludeParam(self):
        filterFile = FilterProvider.getFileName(self.assembly, self.exclude)
        if filterFile:
            return '--exclude {filterFile}'.format(filterFile=filterFile)
        else:
            return ''

    def __init__(self, assembly, select, exclude, hdf5Files, chromSizesFile, outHdf5Files):
        self.assembly = assembly
        self.select = select
        self.exclude = exclude
        self.hdf5Files = hdf5Files
        self.chromSizesFile = chromSizesFile
        self.outHdf5Files = outHdf5Files

    def build(self):
        return [
            EpiGeecFilter.FILTER_TEMPLATE.format(
                select=self.selectParam(),
                exclude=self.excludeParam(),
                hdf5=in_hdf5,
                chromSizes=self.chromSizesFile,
                outHdf5=out_hdf5
            )
            for in_hdf5, out_hdf5 in zip(self.hdf5Files, self.outHdf5Files)
        ]

class EpiGeecCorrelate(object):
    CORRELATE_TEMPLATE = 'epigeec correlate {metric} {hdf5List} {chromSizes} {outMatrix}'
    CORRELATE_TEMPLATE_METRIC = {
        'pearson': '',
        'spearman': '--concat'
    }

    def __init__(self, metric, hdf5ListFile, chromSizesFile, outMatrixFile):
        self.metric = metric
        self.hdf5ListFile = hdf5ListFile
        self.chromSizesFile = chromSizesFile
        self.outMatrixFile = outMatrixFile

    def build(self):
        return [
            EpiGeecCorrelate.CORRELATE_TEMPLATE.format(
                metric=EpiGeecCorrelate.CORRELATE_TEMPLATE_METRIC[self.metric],
                hdf5List=self.hdf5ListFile,
                chromSizes=self.chromSizesFile,
                outMatrix=self.outMatrixFile
            )
        ]

class EpiGeecTrimMd5(object):
    TRIM_MD5_TEMPLATE = 'sed -i s/_{resolution}_{select}_{exclude}_{metric}//g {matrix}'
    TRIM_MD5_TEMPLATE_METRIC = {
        'pearson': 'value',
        'spearman': 'rank'
    }

    def __init__(self, resolution, select, exclude, metric, matrix):
        self.resolution = resolution
        self.select = select
        self.exclude = exclude
        self.metric = metric
        self.matrix = matrix

    def build(self):
        return [
            EpiGeecTrimMd5.TRIM_MD5_TEMPLATE.format(
                resolution = self.resolution,
                select = self.select,
                exclude = self.exclude,
                metric = EpiGeecTrimMd5.TRIM_MD5_TEMPLATE_METRIC[self.metric],
                matrix = self.matrix
            )
        ]
