# epigeec generator

Generate scripts that can be ran on a SLURM system, to create hdf5s.  
Handle resolution/filter/correlation dependencies.  

## Installation

This project depends on `epigeec_converter`, and so that project also needs to be cloned.  

Create the required virtual environment:  
`sh SETUP`  

Install `epigeec_converter` while being in the venv:  
`. epigeec_generator/env/bin/activate`  
`python epigeec_converter/setup.py install`.  

## Usage

### config file

The `config.py` file determines what hdf5 script will be generated. Comment and uncomment lines according to what is needed. Configures:

- assemblies
- filters
- resolutions
- metrics
- chromsizes
- date/release

### main command

The create the scripts, run `python main.py datapath`, where **datapath** is the path of the local IHEC datasource.
Hdf5 file lists, hdf5 files, and matrix files will be generated there.  

When running the last command, **submitter.sh** will be created in the `epigeec_generator` directory, and the associated scripts will be created in the **script** directory.
Hdf5 directories and lists will also be created in **datapath**.  

### Creating hdf5s for a limited number of files, and then re-run correlation

To generate scripts to create hdf5s for a limited number of bigwigs instead of all the ones present in the local IHEC datasource,
the signal and md5sum selection needs to be modified in **main.py**:

~~~python
signals = list(local_src.current_bigwigs_alt)
md5sums = local_src.paths_to_md5s(signals)
~~~

Note that this will also erase the hdf5 lists for selected **config.py** parameters.
Then, **submitter.sh** needs to be modified to not call correlation scripts (erase lines calling *MakeCorrelate* scripts).  

Once the jobs have been completed, **main.py** needs to be ran with the original signal selection (including new signals),
this will recreate the correct hdf5 lists.
Then, **submitter.sh** needs to be modified to only call correlation scripts (erase lines not calling *MakeCorrelate* scripts and remove dependencies).  

### Changing job parameters

To modify script SLURM parameters like the number of cores used, the required memory and the walltime, modify **commandwrapper.py**.  

## Problems and solutions

### Wrong submitter.sh paths

Moving the scripts in the **script** directory to a different path (which is recommended) will break **submitter.sh**. Change the paths in the submitter accordingly.  

### Scripts too big for the SLURM scheduler

Sometimes, the generated scripts will be too big (>4MB) to be accepted by the SLURM scheduler, resulting in an error like the following:

~~~text
sbatch: error: Batch job submission failed: Pathname of a file, directory or other parameter too long.
~~~

To correct that, **utils/shorten_scripts.sh** is provided to shorten all MakeFilter/MakeDownResolution/MakeToRank files from a given directory (non recursively). It does that by replacing paths using variables. This script might need to be modified for a different environment, as it hard-codes certain paths.  

Usage: `sh shorten_scripts.sh directory/to/scripts/ assembly release`.  
