from os.path import abspath
from os.path import dirname
from uuid import uuid4
import distutils.dir_util

class JobSubmitter(object):
    SUBMITTER_TEMPLATE = '''#!/bin/bash
{slurmlines}
'''
    SLURM_LINE = '{jobname}=$(sbatch --parsable {jobscriptname})'
    SLURM_LINE_WITH_DEPENDENCY = '{jobname}=$(sbatch --parsable --dependency=afterok:${dependency} {jobscriptname})'

    def _generate_slurm_line(self, jobname, jobscriptname, depend_of=None):
        if depend_of:
            return JobSubmitter.SLURM_LINE_WITH_DEPENDENCY.format(
                jobname=jobname,
                dependency=',$'.join(depend_of.jobnames),
                jobscriptname=jobscriptname
            )
        else:
            return JobSubmitter.SLURM_LINE.format(
                jobname=jobname,
                jobscriptname=jobscriptname
            )

    def _generate_slurm_lines(self, job, depend_of=None):
        return [
            self._generate_slurm_line(jobname, jobscriptname, depend_of)
            for jobname, jobscriptname in zip(job.jobnames, job.jobscriptnames)
        ]

    def __init__(self):
        self.jobs = []

    def add_job(self, job):
        self.jobs.append(job)

    def _build_job(self, job, depend_of=None):
        job.build()
        slurmlines = self._generate_slurm_lines(job, depend_of)

        for dependent in job.dependent_jobs:
            slurmlines.extend(self._build_job(dependent, job))

        return slurmlines

    def build(self):
        slurmlines = []

        for job in self.jobs:
            slurmlines.extend(self._build_job(job))

        with open('submitter.sh', 'w') as f:
            f.write(
                JobSubmitter.SUBMITTER_TEMPLATE.format(
                    slurmlines='\n'.join(slurmlines)
                )
            )

class MetaJob(object):
    def __init__(self, jobname=None, walltime='24:00:00', cpu_count=1, mem='8G'):
        if not jobname:
            jobname = 'job_{}'.format(uuid4())
        self.jobname = jobname
        self.walltime = walltime
        self.cpu_count = cpu_count
        self.mem = mem
        self.dependent_jobs = []
        self.commands_batch = []

    @property
    def jobscriptnames(self):
        return [
            abspath('script/{jobname}.sh'.format(jobname=jobname))
            for jobname in self.jobnames
        ]

    @property
    def jobnames(self):
        return [
            '{}_{}'.format(self.jobname, i)
            for i in range(len(self.commands_batch))
        ]

    def set_commands_batch(self, commands):
        self.commands_batch = commands

        return self

    def add_dependent_job(self, job):
        self.dependent_jobs.append(job)

        return self

    def add_dependent_jobs(self, jobs):
        self.dependent_jobs.extend(jobs)

        return self

    def build(self):
        for names, cmds in zip(self.jobnames, self.commands_batch):
            Job(
                jobname=names,
                walltime=self.walltime,
                cpu_count=self.cpu_count,
                mem=self.mem
            ).add_commands(
                cmds
            ).add_dependent_jobs(
                self.dependent_jobs
            ).build()

class Job(object):
    JOB_TEMPLATE = '''#!/bin/bash

#SBATCH --account=def-jacquesp
#SBATCH --job-name={jobname}
#SBATCH --output=%x-%j.out
#SBATCH --time={walltime}
#SBATCH --cpus-per-task={cpu_count}
#SBATCH --mem={mem}

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

# ml gcc/5.4.0 boost/1.60.0 hdf5/1.8.18 python/2.7.14 scipy-stack/2017b
ml boost/1.60.0 scipy-stack/2017b

source {env_activate_path}

{commands}
'''

    def __init__(self, jobname=None, walltime='24:00:00', cpu_count=1, mem='8G'):
        if not jobname:
            jobname = 'job_{}'.format(uuid4())
        self.jobname = jobname
        self.walltime = walltime
        self.cpu_count = cpu_count
        self.mem = mem
        self.dependent_jobs = []
        self.commands = []

        self.jobscriptname = abspath('script/{jobname}.sh'.format(jobname=self.jobname))

    @property
    def jobscriptnames(self):
        return [
            self.jobscriptname
        ]

    @property
    def jobnames(self):
        return [
            self.jobname
        ]

    def add_command(self, command):
        self.commands.append(command)

        return self

    def add_commands(self, commands):
        self.commands.extend(commands)

        return self

    def add_dependent_job(self, job):
        self.dependent_jobs.append(job)

        return self

    def add_dependent_jobs(self, jobs):
        self.dependent_jobs.extend(jobs)

        return self

    def _build(self, command_order):
        """Write scripts in ./scripts/ folder, using
        the given command order.
        """
        distutils.dir_util.mkpath(dirname(self.jobscriptname))
        with open(self.jobscriptname, 'w') as f:
            f.write(
                self.JOB_TEMPLATE.format(
                    jobname=self.jobname,
                    walltime=self.walltime,
                    cpu_count=self.cpu_count,
                    mem=self.mem,
                    env_activate_path=abspath('env/bin/activate'),
                    commands='\n'.join(command_order)
                )
            )

    def build(self):
        """Write scripts for given commands in sequence.
        Calls command.build() in the received order of command objects.
        """
        in_sequence = [
            cmdline
            for command in self.commands
            for cmdline in command.build()
            ]
        self._build(in_sequence)

    def build_alternate(self):
        """Write scripts for given commands in an alternating order.
        Given commands need to have the same number of non-zero calls.
        """
        alternate = []
        all_builds = [command.build() for command in self.commands]
        nb_calls = len(all_builds[0])
        for i in range(nb_calls):
            for build in all_builds:
                alternate.append(build[i])
        self._build(alternate)
